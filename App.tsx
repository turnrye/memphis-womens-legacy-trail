import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Women of Achievement</Text>
      <Text>The Memphis Women’s Legacy Trail is a project to document, remember, and celebrate the women of Memphis, Tennessee that have made a lasting impact on the city through their work and lives. Compiled by Dr. Beverly Bond, Dr. Margaret Caffrey, Judy Card, Deborah Clubb, Dr. Gail Murray, Jimmy Ogle, and Laura Todd, the brochure and information is the first of its kind in Memphis. Our goal is to raise funds to expand the project in hopes to share the stories, history, and vision of these remarkable Memphis women.</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
